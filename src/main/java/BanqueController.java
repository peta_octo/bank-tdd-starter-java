public class BanqueController {
    private final Console console;

    public BanqueController(Console console) {
        this.console = console;
    }

    public void interpréter(String entrée) {
        throw new UnsupportedOperationException("À vous de jouer !");
    }

    public void afficherMessageDeBienvenue() {
        console.afficher("Bienvenue chez vous! Votre argent est en sécurité chez TDD-Bank, votre banque conçue en TDD.");
    }
}
