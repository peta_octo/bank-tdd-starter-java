import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String entrée;
        var banqueController = new BanqueController(new Console());

        banqueController.afficherMessageDeBienvenue();

        do {
            System.out.print("Saisir une commande à effectuer sur votre compte (ou 'quitter' pour partir): ");
            entrée = scanner.nextLine();

            if (entrée.equals("quitter")) {
                break;
            }

            banqueController.interpréter(entrée);

        } while (true);

        System.out.println("À bientôt !");

        scanner.close();
    }
}
