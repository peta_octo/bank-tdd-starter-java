import org.junit.Test;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;


public class BanqueControllerTest {

    @Test
    public void affiche_le_message_de_bienvenue_de_la_banque() {
        var console = mock(Console.class);
        var controller = new BanqueController(console);

        controller.afficherMessageDeBienvenue();

        verify(console).afficher("Bienvenue chez vous! Votre argent est en sécurité chez OCTO Bank");
    }
}
